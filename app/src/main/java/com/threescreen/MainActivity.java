package com.threescreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button btn_screen_one=(Button) findViewById(R.id.btn_screen1);
        Button btn_screen_two=(Button) findViewById(R.id.btn_screen2);
        Button btn_screen_three=(Button) findViewById(R.id.btn_screen3);

        btn_screen_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Screen_one.class);
                startActivity(intent);
            }
        });

        btn_screen_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Screen_two.class);
                startActivity(intent);
            }
        });

        btn_screen_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Screen_three.class);
                startActivity(intent);
            }
        });
    }

}